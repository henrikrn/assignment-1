/**
 */
package henrik.studyPlan;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Department</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link henrik.studyPlan.Department#getName <em>Name</em>}</li>
 *   <li>{@link henrik.studyPlan.Department#getNoOfCourses <em>No Of Courses</em>}</li>
 *   <li>{@link henrik.studyPlan.Department#getCourses <em>Courses</em>}</li>
 *   <li>{@link henrik.studyPlan.Department#getStudyPrograms <em>Study Programs</em>}</li>
 * </ul>
 *
 * @see henrik.studyPlan.StudyPlanPackage#getDepartment()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='missingCourses missingStudyPrograms'"
 * @generated
 */
public interface Department extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see henrik.studyPlan.StudyPlanPackage#getDepartment_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link henrik.studyPlan.Department#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>No Of Courses</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>No Of Courses</em>' attribute.
	 * @see henrik.studyPlan.StudyPlanPackage#getDepartment_NoOfCourses()
	 * @model default="0" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	int getNoOfCourses();

	/**
	 * Returns the value of the '<em><b>Courses</b></em>' containment reference list.
	 * The list contents are of type {@link henrik.studyPlan.Course}.
	 * It is bidirectional and its opposite is '{@link henrik.studyPlan.Course#getOwnedBy <em>Owned By</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Courses</em>' containment reference list.
	 * @see henrik.studyPlan.StudyPlanPackage#getDepartment_Courses()
	 * @see henrik.studyPlan.Course#getOwnedBy
	 * @model opposite="ownedBy" containment="true"
	 * @generated
	 */
	EList<Course> getCourses();

	/**
	 * Returns the value of the '<em><b>Study Programs</b></em>' containment reference list.
	 * The list contents are of type {@link henrik.studyPlan.StudyProgram}.
	 * It is bidirectional and its opposite is '{@link henrik.studyPlan.StudyProgram#getDepartment <em>Department</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Study Programs</em>' containment reference list.
	 * @see henrik.studyPlan.StudyPlanPackage#getDepartment_StudyPrograms()
	 * @see henrik.studyPlan.StudyProgram#getDepartment
	 * @model opposite="department" containment="true"
	 * @generated
	 */
	EList<StudyProgram> getStudyPrograms();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model CourseRequired="true"
	 * @generated
	 */
	void addCourse(Course Course);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model StudyProgramRequired="true"
	 * @generated
	 */
	void addStudyProgram(StudyProgram StudyProgram);

} // Department
