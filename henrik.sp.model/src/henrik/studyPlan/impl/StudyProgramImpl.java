/**
 */
package henrik.studyPlan.impl;

import henrik.studyPlan.Degree;
import henrik.studyPlan.Department;
import henrik.studyPlan.Semester;
import henrik.studyPlan.StudyPlanPackage;
import henrik.studyPlan.StudyProgram;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Study Program</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link henrik.studyPlan.impl.StudyProgramImpl#getName <em>Name</em>}</li>
 *   <li>{@link henrik.studyPlan.impl.StudyProgramImpl#getCode <em>Code</em>}</li>
 *   <li>{@link henrik.studyPlan.impl.StudyProgramImpl#getDegree <em>Degree</em>}</li>
 *   <li>{@link henrik.studyPlan.impl.StudyProgramImpl#getNoOfSemesters <em>No Of Semesters</em>}</li>
 *   <li>{@link henrik.studyPlan.impl.StudyProgramImpl#getSemesters <em>Semesters</em>}</li>
 *   <li>{@link henrik.studyPlan.impl.StudyProgramImpl#getDepartment <em>Department</em>}</li>
 * </ul>
 *
 * @generated
 */
public class StudyProgramImpl extends MinimalEObjectImpl.Container implements StudyProgram {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getCode() <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCode()
	 * @generated
	 * @ordered
	 */
	protected static final String CODE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCode() <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCode()
	 * @generated
	 * @ordered
	 */
	protected String code = CODE_EDEFAULT;

	/**
	 * The default value of the '{@link #getDegree() <em>Degree</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDegree()
	 * @generated
	 * @ordered
	 */
	protected static final Degree DEGREE_EDEFAULT = Degree.BACHELOR;

	/**
	 * The cached value of the '{@link #getDegree() <em>Degree</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDegree()
	 * @generated
	 * @ordered
	 */
	protected Degree degree = DEGREE_EDEFAULT;

	/**
	 * The default value of the '{@link #getNoOfSemesters() <em>No Of Semesters</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNoOfSemesters()
	 * @generated
	 * @ordered
	 */
	protected static final int NO_OF_SEMESTERS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getNoOfSemesters() <em>No Of Semesters</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNoOfSemesters()
	 * @generated
	 * @ordered
	 */
	protected int noOfSemesters = NO_OF_SEMESTERS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSemesters() <em>Semesters</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSemesters()
	 * @generated
	 * @ordered
	 */
	protected EList<Semester> semesters;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StudyProgramImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StudyPlanPackage.Literals.STUDY_PROGRAM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StudyPlanPackage.STUDY_PROGRAM__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCode() {
		return code;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCode(String newCode) {
		String oldCode = code;
		code = newCode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StudyPlanPackage.STUDY_PROGRAM__CODE, oldCode, code));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Degree getDegree() {
		return degree;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDegree(Degree newDegree) {
		Degree oldDegree = degree;
		degree = newDegree == null ? DEGREE_EDEFAULT : newDegree;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StudyPlanPackage.STUDY_PROGRAM__DEGREE, oldDegree, degree));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getNoOfSemesters() {
		return noOfSemesters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNoOfSemesters(int newNoOfSemesters) {
		int oldNoOfSemesters = noOfSemesters;
		noOfSemesters = newNoOfSemesters;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StudyPlanPackage.STUDY_PROGRAM__NO_OF_SEMESTERS, oldNoOfSemesters, noOfSemesters));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Semester> getSemesters() {
		if (semesters == null) {
			semesters = new EObjectContainmentEList<Semester>(Semester.class, this, StudyPlanPackage.STUDY_PROGRAM__SEMESTERS);
		}
		return semesters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Department getDepartment() {
		if (eContainerFeatureID() != StudyPlanPackage.STUDY_PROGRAM__DEPARTMENT) return null;
		return (Department)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDepartment(Department newDepartment, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newDepartment, StudyPlanPackage.STUDY_PROGRAM__DEPARTMENT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDepartment(Department newDepartment) {
		if (newDepartment != eInternalContainer() || (eContainerFeatureID() != StudyPlanPackage.STUDY_PROGRAM__DEPARTMENT && newDepartment != null)) {
			if (EcoreUtil.isAncestor(this, newDepartment))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newDepartment != null)
				msgs = ((InternalEObject)newDepartment).eInverseAdd(this, StudyPlanPackage.DEPARTMENT__STUDY_PROGRAMS, Department.class, msgs);
			msgs = basicSetDepartment(newDepartment, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StudyPlanPackage.STUDY_PROGRAM__DEPARTMENT, newDepartment, newDepartment));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StudyPlanPackage.STUDY_PROGRAM__DEPARTMENT:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetDepartment((Department)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StudyPlanPackage.STUDY_PROGRAM__SEMESTERS:
				return ((InternalEList<?>)getSemesters()).basicRemove(otherEnd, msgs);
			case StudyPlanPackage.STUDY_PROGRAM__DEPARTMENT:
				return basicSetDepartment(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case StudyPlanPackage.STUDY_PROGRAM__DEPARTMENT:
				return eInternalContainer().eInverseRemove(this, StudyPlanPackage.DEPARTMENT__STUDY_PROGRAMS, Department.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StudyPlanPackage.STUDY_PROGRAM__NAME:
				return getName();
			case StudyPlanPackage.STUDY_PROGRAM__CODE:
				return getCode();
			case StudyPlanPackage.STUDY_PROGRAM__DEGREE:
				return getDegree();
			case StudyPlanPackage.STUDY_PROGRAM__NO_OF_SEMESTERS:
				return getNoOfSemesters();
			case StudyPlanPackage.STUDY_PROGRAM__SEMESTERS:
				return getSemesters();
			case StudyPlanPackage.STUDY_PROGRAM__DEPARTMENT:
				return getDepartment();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StudyPlanPackage.STUDY_PROGRAM__NAME:
				setName((String)newValue);
				return;
			case StudyPlanPackage.STUDY_PROGRAM__CODE:
				setCode((String)newValue);
				return;
			case StudyPlanPackage.STUDY_PROGRAM__DEGREE:
				setDegree((Degree)newValue);
				return;
			case StudyPlanPackage.STUDY_PROGRAM__NO_OF_SEMESTERS:
				setNoOfSemesters((Integer)newValue);
				return;
			case StudyPlanPackage.STUDY_PROGRAM__SEMESTERS:
				getSemesters().clear();
				getSemesters().addAll((Collection<? extends Semester>)newValue);
				return;
			case StudyPlanPackage.STUDY_PROGRAM__DEPARTMENT:
				setDepartment((Department)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StudyPlanPackage.STUDY_PROGRAM__NAME:
				setName(NAME_EDEFAULT);
				return;
			case StudyPlanPackage.STUDY_PROGRAM__CODE:
				setCode(CODE_EDEFAULT);
				return;
			case StudyPlanPackage.STUDY_PROGRAM__DEGREE:
				setDegree(DEGREE_EDEFAULT);
				return;
			case StudyPlanPackage.STUDY_PROGRAM__NO_OF_SEMESTERS:
				setNoOfSemesters(NO_OF_SEMESTERS_EDEFAULT);
				return;
			case StudyPlanPackage.STUDY_PROGRAM__SEMESTERS:
				getSemesters().clear();
				return;
			case StudyPlanPackage.STUDY_PROGRAM__DEPARTMENT:
				setDepartment((Department)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StudyPlanPackage.STUDY_PROGRAM__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case StudyPlanPackage.STUDY_PROGRAM__CODE:
				return CODE_EDEFAULT == null ? code != null : !CODE_EDEFAULT.equals(code);
			case StudyPlanPackage.STUDY_PROGRAM__DEGREE:
				return degree != DEGREE_EDEFAULT;
			case StudyPlanPackage.STUDY_PROGRAM__NO_OF_SEMESTERS:
				return noOfSemesters != NO_OF_SEMESTERS_EDEFAULT;
			case StudyPlanPackage.STUDY_PROGRAM__SEMESTERS:
				return semesters != null && !semesters.isEmpty();
			case StudyPlanPackage.STUDY_PROGRAM__DEPARTMENT:
				return getDepartment() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", code: ");
		result.append(code);
		result.append(", degree: ");
		result.append(degree);
		result.append(", noOfSemesters: ");
		result.append(noOfSemesters);
		result.append(')');
		return result.toString();
	}

} //StudyProgramImpl
