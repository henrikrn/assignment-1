/**
 */
package henrik.studyPlan.impl;

import henrik.studyPlan.Course;
import henrik.studyPlan.Department;
import henrik.studyPlan.StudyPlanPackage;
import henrik.studyPlan.StudyProgram;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Department</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link henrik.studyPlan.impl.DepartmentImpl#getName <em>Name</em>}</li>
 *   <li>{@link henrik.studyPlan.impl.DepartmentImpl#getNoOfCourses <em>No Of Courses</em>}</li>
 *   <li>{@link henrik.studyPlan.impl.DepartmentImpl#getCourses <em>Courses</em>}</li>
 *   <li>{@link henrik.studyPlan.impl.DepartmentImpl#getStudyPrograms <em>Study Programs</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DepartmentImpl extends MinimalEObjectImpl.Container implements Department {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getNoOfCourses() <em>No Of Courses</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNoOfCourses()
	 * @generated
	 * @ordered
	 */
	protected static final int NO_OF_COURSES_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getCourses() <em>Courses</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourses()
	 * @generated
	 * @ordered
	 */
	protected EList<Course> courses;

	/**
	 * The cached value of the '{@link #getStudyPrograms() <em>Study Programs</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStudyPrograms()
	 * @generated
	 * @ordered
	 */
	protected EList<StudyProgram> studyPrograms;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DepartmentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StudyPlanPackage.Literals.DEPARTMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StudyPlanPackage.DEPARTMENT__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int getNoOfCourses() {
		return getCourses().size();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Course> getCourses() {
		if (courses == null) {
			courses = new EObjectContainmentWithInverseEList<Course>(Course.class, this, StudyPlanPackage.DEPARTMENT__COURSES, StudyPlanPackage.COURSE__OWNED_BY);
		}
		return courses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<StudyProgram> getStudyPrograms() {
		if (studyPrograms == null) {
			studyPrograms = new EObjectContainmentWithInverseEList<StudyProgram>(StudyProgram.class, this, StudyPlanPackage.DEPARTMENT__STUDY_PROGRAMS, StudyPlanPackage.STUDY_PROGRAM__DEPARTMENT);
		}
		return studyPrograms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void addCourse(Course Course) {
		getCourses().add(Course);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void addStudyProgram(StudyProgram StudyProgram) {
		getStudyPrograms().add(StudyProgram);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StudyPlanPackage.DEPARTMENT__COURSES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getCourses()).basicAdd(otherEnd, msgs);
			case StudyPlanPackage.DEPARTMENT__STUDY_PROGRAMS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getStudyPrograms()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StudyPlanPackage.DEPARTMENT__COURSES:
				return ((InternalEList<?>)getCourses()).basicRemove(otherEnd, msgs);
			case StudyPlanPackage.DEPARTMENT__STUDY_PROGRAMS:
				return ((InternalEList<?>)getStudyPrograms()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StudyPlanPackage.DEPARTMENT__NAME:
				return getName();
			case StudyPlanPackage.DEPARTMENT__NO_OF_COURSES:
				return getNoOfCourses();
			case StudyPlanPackage.DEPARTMENT__COURSES:
				return getCourses();
			case StudyPlanPackage.DEPARTMENT__STUDY_PROGRAMS:
				return getStudyPrograms();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StudyPlanPackage.DEPARTMENT__NAME:
				setName((String)newValue);
				return;
			case StudyPlanPackage.DEPARTMENT__COURSES:
				getCourses().clear();
				getCourses().addAll((Collection<? extends Course>)newValue);
				return;
			case StudyPlanPackage.DEPARTMENT__STUDY_PROGRAMS:
				getStudyPrograms().clear();
				getStudyPrograms().addAll((Collection<? extends StudyProgram>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StudyPlanPackage.DEPARTMENT__NAME:
				setName(NAME_EDEFAULT);
				return;
			case StudyPlanPackage.DEPARTMENT__COURSES:
				getCourses().clear();
				return;
			case StudyPlanPackage.DEPARTMENT__STUDY_PROGRAMS:
				getStudyPrograms().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StudyPlanPackage.DEPARTMENT__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case StudyPlanPackage.DEPARTMENT__NO_OF_COURSES:
				return getNoOfCourses() != NO_OF_COURSES_EDEFAULT;
			case StudyPlanPackage.DEPARTMENT__COURSES:
				return courses != null && !courses.isEmpty();
			case StudyPlanPackage.DEPARTMENT__STUDY_PROGRAMS:
				return studyPrograms != null && !studyPrograms.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case StudyPlanPackage.DEPARTMENT___ADD_COURSE__COURSE:
				addCourse((Course)arguments.get(0));
				return null;
			case StudyPlanPackage.DEPARTMENT___ADD_STUDY_PROGRAM__STUDYPROGRAM:
				addStudyProgram((StudyProgram)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //DepartmentImpl
