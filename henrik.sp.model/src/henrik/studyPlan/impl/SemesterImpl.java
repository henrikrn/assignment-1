/**
 */
package henrik.studyPlan.impl;

import henrik.studyPlan.Course;
import henrik.studyPlan.Semester;
import henrik.studyPlan.Specialization;
import henrik.studyPlan.StudyPlanPackage;
import henrik.studyPlan.Term;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Semester</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link henrik.studyPlan.impl.SemesterImpl#getSemesterNo <em>Semester No</em>}</li>
 *   <li>{@link henrik.studyPlan.impl.SemesterImpl#getTerm <em>Term</em>}</li>
 *   <li>{@link henrik.studyPlan.impl.SemesterImpl#getSpecialization <em>Specialization</em>}</li>
 *   <li>{@link henrik.studyPlan.impl.SemesterImpl#getMandatoryCourses <em>Mandatory Courses</em>}</li>
 *   <li>{@link henrik.studyPlan.impl.SemesterImpl#getElectiveCourses <em>Elective Courses</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SemesterImpl extends MinimalEObjectImpl.Container implements Semester {
	/**
	 * The default value of the '{@link #getSemesterNo() <em>Semester No</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSemesterNo()
	 * @generated
	 * @ordered
	 */
	protected static final int SEMESTER_NO_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getSemesterNo() <em>Semester No</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSemesterNo()
	 * @generated
	 * @ordered
	 */
	protected int semesterNo = SEMESTER_NO_EDEFAULT;

	/**
	 * The default value of the '{@link #getTerm() <em>Term</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTerm()
	 * @generated
	 * @ordered
	 */
	protected static final Specialization TERM_EDEFAULT = Specialization.SOFTWARE_SYSTEMS;

	/**
	 * The cached value of the '{@link #getTerm() <em>Term</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTerm()
	 * @generated
	 * @ordered
	 */
	protected Specialization term = TERM_EDEFAULT;

	/**
	 * The default value of the '{@link #getSpecialization() <em>Specialization</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecialization()
	 * @generated
	 * @ordered
	 */
	protected static final Term SPECIALIZATION_EDEFAULT = Term.SPRING;

	/**
	 * The cached value of the '{@link #getSpecialization() <em>Specialization</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecialization()
	 * @generated
	 * @ordered
	 */
	protected Term specialization = SPECIALIZATION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getMandatoryCourses() <em>Mandatory Courses</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMandatoryCourses()
	 * @generated
	 * @ordered
	 */
	protected EList<Course> mandatoryCourses;

	/**
	 * The cached value of the '{@link #getElectiveCourses() <em>Elective Courses</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElectiveCourses()
	 * @generated
	 * @ordered
	 */
	protected EList<Course> electiveCourses;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SemesterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StudyPlanPackage.Literals.SEMESTER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getSemesterNo() {
		return semesterNo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSemesterNo(int newSemesterNo) {
		int oldSemesterNo = semesterNo;
		semesterNo = newSemesterNo;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StudyPlanPackage.SEMESTER__SEMESTER_NO, oldSemesterNo, semesterNo));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Specialization getTerm() {
		return term;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTerm(Specialization newTerm) {
		Specialization oldTerm = term;
		term = newTerm == null ? TERM_EDEFAULT : newTerm;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StudyPlanPackage.SEMESTER__TERM, oldTerm, term));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Term getSpecialization() {
		return specialization;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSpecialization(Term newSpecialization) {
		Term oldSpecialization = specialization;
		specialization = newSpecialization == null ? SPECIALIZATION_EDEFAULT : newSpecialization;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StudyPlanPackage.SEMESTER__SPECIALIZATION, oldSpecialization, specialization));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Course> getMandatoryCourses() {
		if (mandatoryCourses == null) {
			mandatoryCourses = new EObjectResolvingEList<Course>(Course.class, this, StudyPlanPackage.SEMESTER__MANDATORY_COURSES);
		}
		return mandatoryCourses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Course> getElectiveCourses() {
		if (electiveCourses == null) {
			electiveCourses = new EObjectResolvingEList<Course>(Course.class, this, StudyPlanPackage.SEMESTER__ELECTIVE_COURSES);
		}
		return electiveCourses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void addMandatoryCourse(Course Course) {
		getMandatoryCourses().add(Course);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void addElectiveCourse(Course Course) {
		getElectiveCourses().add(Course);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StudyPlanPackage.SEMESTER__SEMESTER_NO:
				return getSemesterNo();
			case StudyPlanPackage.SEMESTER__TERM:
				return getTerm();
			case StudyPlanPackage.SEMESTER__SPECIALIZATION:
				return getSpecialization();
			case StudyPlanPackage.SEMESTER__MANDATORY_COURSES:
				return getMandatoryCourses();
			case StudyPlanPackage.SEMESTER__ELECTIVE_COURSES:
				return getElectiveCourses();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StudyPlanPackage.SEMESTER__SEMESTER_NO:
				setSemesterNo((Integer)newValue);
				return;
			case StudyPlanPackage.SEMESTER__TERM:
				setTerm((Specialization)newValue);
				return;
			case StudyPlanPackage.SEMESTER__SPECIALIZATION:
				setSpecialization((Term)newValue);
				return;
			case StudyPlanPackage.SEMESTER__MANDATORY_COURSES:
				getMandatoryCourses().clear();
				getMandatoryCourses().addAll((Collection<? extends Course>)newValue);
				return;
			case StudyPlanPackage.SEMESTER__ELECTIVE_COURSES:
				getElectiveCourses().clear();
				getElectiveCourses().addAll((Collection<? extends Course>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StudyPlanPackage.SEMESTER__SEMESTER_NO:
				setSemesterNo(SEMESTER_NO_EDEFAULT);
				return;
			case StudyPlanPackage.SEMESTER__TERM:
				setTerm(TERM_EDEFAULT);
				return;
			case StudyPlanPackage.SEMESTER__SPECIALIZATION:
				setSpecialization(SPECIALIZATION_EDEFAULT);
				return;
			case StudyPlanPackage.SEMESTER__MANDATORY_COURSES:
				getMandatoryCourses().clear();
				return;
			case StudyPlanPackage.SEMESTER__ELECTIVE_COURSES:
				getElectiveCourses().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StudyPlanPackage.SEMESTER__SEMESTER_NO:
				return semesterNo != SEMESTER_NO_EDEFAULT;
			case StudyPlanPackage.SEMESTER__TERM:
				return term != TERM_EDEFAULT;
			case StudyPlanPackage.SEMESTER__SPECIALIZATION:
				return specialization != SPECIALIZATION_EDEFAULT;
			case StudyPlanPackage.SEMESTER__MANDATORY_COURSES:
				return mandatoryCourses != null && !mandatoryCourses.isEmpty();
			case StudyPlanPackage.SEMESTER__ELECTIVE_COURSES:
				return electiveCourses != null && !electiveCourses.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case StudyPlanPackage.SEMESTER___ADD_MANDATORY_COURSE__COURSE:
				addMandatoryCourse((Course)arguments.get(0));
				return null;
			case StudyPlanPackage.SEMESTER___ADD_ELECTIVE_COURSE__COURSE:
				addElectiveCourse((Course)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (semesterNo: ");
		result.append(semesterNo);
		result.append(", term: ");
		result.append(term);
		result.append(", specialization: ");
		result.append(specialization);
		result.append(')');
		return result.toString();
	}

} //SemesterImpl
