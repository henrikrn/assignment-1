/**
 */
package henrik.studyPlan;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see henrik.studyPlan.StudyPlanFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore validationDelegates='http://www.eclipse.org/acceleo/query/1.0'"
 * @generated
 */
public interface StudyPlanPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "studyPlan";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "platform:/plugin/henrik.sp.model/model/sp.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "studyPlan";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	StudyPlanPackage eINSTANCE = henrik.studyPlan.impl.StudyPlanPackageImpl.init();

	/**
	 * The meta object id for the '{@link henrik.studyPlan.impl.CourseImpl <em>Course</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see henrik.studyPlan.impl.CourseImpl
	 * @see henrik.studyPlan.impl.StudyPlanPackageImpl#getCourse()
	 * @generated
	 */
	int COURSE = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__CODE = 1;

	/**
	 * The feature id for the '<em><b>Study Points</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__STUDY_POINTS = 2;

	/**
	 * The feature id for the '<em><b>Owned By</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__OWNED_BY = 3;

	/**
	 * The number of structural features of the '<em>Course</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Course</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link henrik.studyPlan.impl.SemesterImpl <em>Semester</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see henrik.studyPlan.impl.SemesterImpl
	 * @see henrik.studyPlan.impl.StudyPlanPackageImpl#getSemester()
	 * @generated
	 */
	int SEMESTER = 1;

	/**
	 * The feature id for the '<em><b>Semester No</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEMESTER__SEMESTER_NO = 0;

	/**
	 * The feature id for the '<em><b>Term</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEMESTER__TERM = 1;

	/**
	 * The feature id for the '<em><b>Specialization</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEMESTER__SPECIALIZATION = 2;

	/**
	 * The feature id for the '<em><b>Mandatory Courses</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEMESTER__MANDATORY_COURSES = 3;

	/**
	 * The feature id for the '<em><b>Elective Courses</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEMESTER__ELECTIVE_COURSES = 4;

	/**
	 * The number of structural features of the '<em>Semester</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEMESTER_FEATURE_COUNT = 5;

	/**
	 * The operation id for the '<em>Add Mandatory Course</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEMESTER___ADD_MANDATORY_COURSE__COURSE = 0;

	/**
	 * The operation id for the '<em>Add Elective Course</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEMESTER___ADD_ELECTIVE_COURSE__COURSE = 1;

	/**
	 * The number of operations of the '<em>Semester</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEMESTER_OPERATION_COUNT = 2;

	/**
	 * The meta object id for the '{@link henrik.studyPlan.impl.StudyProgramImpl <em>Study Program</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see henrik.studyPlan.impl.StudyProgramImpl
	 * @see henrik.studyPlan.impl.StudyPlanPackageImpl#getStudyProgram()
	 * @generated
	 */
	int STUDY_PROGRAM = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY_PROGRAM__NAME = 0;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY_PROGRAM__CODE = 1;

	/**
	 * The feature id for the '<em><b>Degree</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY_PROGRAM__DEGREE = 2;

	/**
	 * The feature id for the '<em><b>No Of Semesters</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY_PROGRAM__NO_OF_SEMESTERS = 3;

	/**
	 * The feature id for the '<em><b>Semesters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY_PROGRAM__SEMESTERS = 4;

	/**
	 * The feature id for the '<em><b>Department</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY_PROGRAM__DEPARTMENT = 5;

	/**
	 * The number of structural features of the '<em>Study Program</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY_PROGRAM_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Study Program</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY_PROGRAM_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link henrik.studyPlan.impl.DepartmentImpl <em>Department</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see henrik.studyPlan.impl.DepartmentImpl
	 * @see henrik.studyPlan.impl.StudyPlanPackageImpl#getDepartment()
	 * @generated
	 */
	int DEPARTMENT = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT__NAME = 0;

	/**
	 * The feature id for the '<em><b>No Of Courses</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT__NO_OF_COURSES = 1;

	/**
	 * The feature id for the '<em><b>Courses</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT__COURSES = 2;

	/**
	 * The feature id for the '<em><b>Study Programs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT__STUDY_PROGRAMS = 3;

	/**
	 * The number of structural features of the '<em>Department</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT_FEATURE_COUNT = 4;

	/**
	 * The operation id for the '<em>Add Course</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT___ADD_COURSE__COURSE = 0;

	/**
	 * The operation id for the '<em>Add Study Program</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT___ADD_STUDY_PROGRAM__STUDYPROGRAM = 1;

	/**
	 * The number of operations of the '<em>Department</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT_OPERATION_COUNT = 2;

	/**
	 * The meta object id for the '{@link henrik.studyPlan.Specialization <em>Specialization</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see henrik.studyPlan.Specialization
	 * @see henrik.studyPlan.impl.StudyPlanPackageImpl#getSpecialization()
	 * @generated
	 */
	int SPECIALIZATION = 4;

	/**
	 * The meta object id for the '{@link henrik.studyPlan.Term <em>Term</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see henrik.studyPlan.Term
	 * @see henrik.studyPlan.impl.StudyPlanPackageImpl#getTerm()
	 * @generated
	 */
	int TERM = 5;

	/**
	 * The meta object id for the '{@link henrik.studyPlan.Degree <em>Degree</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see henrik.studyPlan.Degree
	 * @see henrik.studyPlan.impl.StudyPlanPackageImpl#getDegree()
	 * @generated
	 */
	int DEGREE = 6;


	/**
	 * The meta object id for the '<em>Course Code</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see henrik.studyPlan.impl.StudyPlanPackageImpl#getCourseCode()
	 * @generated
	 */
	int COURSE_CODE = 7;


	/**
	 * Returns the meta object for class '{@link henrik.studyPlan.Course <em>Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Course</em>'.
	 * @see henrik.studyPlan.Course
	 * @generated
	 */
	EClass getCourse();

	/**
	 * Returns the meta object for the attribute '{@link henrik.studyPlan.Course#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see henrik.studyPlan.Course#getName()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_Name();

	/**
	 * Returns the meta object for the attribute '{@link henrik.studyPlan.Course#getCode <em>Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Code</em>'.
	 * @see henrik.studyPlan.Course#getCode()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_Code();

	/**
	 * Returns the meta object for the attribute list '{@link henrik.studyPlan.Course#getStudyPoints <em>Study Points</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Study Points</em>'.
	 * @see henrik.studyPlan.Course#getStudyPoints()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_StudyPoints();

	/**
	 * Returns the meta object for the container reference '{@link henrik.studyPlan.Course#getOwnedBy <em>Owned By</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Owned By</em>'.
	 * @see henrik.studyPlan.Course#getOwnedBy()
	 * @see #getCourse()
	 * @generated
	 */
	EReference getCourse_OwnedBy();

	/**
	 * Returns the meta object for class '{@link henrik.studyPlan.Semester <em>Semester</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Semester</em>'.
	 * @see henrik.studyPlan.Semester
	 * @generated
	 */
	EClass getSemester();

	/**
	 * Returns the meta object for the attribute '{@link henrik.studyPlan.Semester#getSemesterNo <em>Semester No</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Semester No</em>'.
	 * @see henrik.studyPlan.Semester#getSemesterNo()
	 * @see #getSemester()
	 * @generated
	 */
	EAttribute getSemester_SemesterNo();

	/**
	 * Returns the meta object for the attribute '{@link henrik.studyPlan.Semester#getTerm <em>Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Term</em>'.
	 * @see henrik.studyPlan.Semester#getTerm()
	 * @see #getSemester()
	 * @generated
	 */
	EAttribute getSemester_Term();

	/**
	 * Returns the meta object for the attribute '{@link henrik.studyPlan.Semester#getSpecialization <em>Specialization</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Specialization</em>'.
	 * @see henrik.studyPlan.Semester#getSpecialization()
	 * @see #getSemester()
	 * @generated
	 */
	EAttribute getSemester_Specialization();

	/**
	 * Returns the meta object for the reference list '{@link henrik.studyPlan.Semester#getMandatoryCourses <em>Mandatory Courses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Mandatory Courses</em>'.
	 * @see henrik.studyPlan.Semester#getMandatoryCourses()
	 * @see #getSemester()
	 * @generated
	 */
	EReference getSemester_MandatoryCourses();

	/**
	 * Returns the meta object for the reference list '{@link henrik.studyPlan.Semester#getElectiveCourses <em>Elective Courses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Elective Courses</em>'.
	 * @see henrik.studyPlan.Semester#getElectiveCourses()
	 * @see #getSemester()
	 * @generated
	 */
	EReference getSemester_ElectiveCourses();

	/**
	 * Returns the meta object for the '{@link henrik.studyPlan.Semester#addMandatoryCourse(henrik.studyPlan.Course) <em>Add Mandatory Course</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Mandatory Course</em>' operation.
	 * @see henrik.studyPlan.Semester#addMandatoryCourse(henrik.studyPlan.Course)
	 * @generated
	 */
	EOperation getSemester__AddMandatoryCourse__Course();

	/**
	 * Returns the meta object for the '{@link henrik.studyPlan.Semester#addElectiveCourse(henrik.studyPlan.Course) <em>Add Elective Course</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Elective Course</em>' operation.
	 * @see henrik.studyPlan.Semester#addElectiveCourse(henrik.studyPlan.Course)
	 * @generated
	 */
	EOperation getSemester__AddElectiveCourse__Course();

	/**
	 * Returns the meta object for class '{@link henrik.studyPlan.StudyProgram <em>Study Program</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Study Program</em>'.
	 * @see henrik.studyPlan.StudyProgram
	 * @generated
	 */
	EClass getStudyProgram();

	/**
	 * Returns the meta object for the attribute '{@link henrik.studyPlan.StudyProgram#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see henrik.studyPlan.StudyProgram#getName()
	 * @see #getStudyProgram()
	 * @generated
	 */
	EAttribute getStudyProgram_Name();

	/**
	 * Returns the meta object for the attribute '{@link henrik.studyPlan.StudyProgram#getCode <em>Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Code</em>'.
	 * @see henrik.studyPlan.StudyProgram#getCode()
	 * @see #getStudyProgram()
	 * @generated
	 */
	EAttribute getStudyProgram_Code();

	/**
	 * Returns the meta object for the attribute '{@link henrik.studyPlan.StudyProgram#getDegree <em>Degree</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Degree</em>'.
	 * @see henrik.studyPlan.StudyProgram#getDegree()
	 * @see #getStudyProgram()
	 * @generated
	 */
	EAttribute getStudyProgram_Degree();

	/**
	 * Returns the meta object for the attribute '{@link henrik.studyPlan.StudyProgram#getNoOfSemesters <em>No Of Semesters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>No Of Semesters</em>'.
	 * @see henrik.studyPlan.StudyProgram#getNoOfSemesters()
	 * @see #getStudyProgram()
	 * @generated
	 */
	EAttribute getStudyProgram_NoOfSemesters();

	/**
	 * Returns the meta object for the containment reference list '{@link henrik.studyPlan.StudyProgram#getSemesters <em>Semesters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Semesters</em>'.
	 * @see henrik.studyPlan.StudyProgram#getSemesters()
	 * @see #getStudyProgram()
	 * @generated
	 */
	EReference getStudyProgram_Semesters();

	/**
	 * Returns the meta object for the container reference '{@link henrik.studyPlan.StudyProgram#getDepartment <em>Department</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Department</em>'.
	 * @see henrik.studyPlan.StudyProgram#getDepartment()
	 * @see #getStudyProgram()
	 * @generated
	 */
	EReference getStudyProgram_Department();

	/**
	 * Returns the meta object for class '{@link henrik.studyPlan.Department <em>Department</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Department</em>'.
	 * @see henrik.studyPlan.Department
	 * @generated
	 */
	EClass getDepartment();

	/**
	 * Returns the meta object for the attribute '{@link henrik.studyPlan.Department#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see henrik.studyPlan.Department#getName()
	 * @see #getDepartment()
	 * @generated
	 */
	EAttribute getDepartment_Name();

	/**
	 * Returns the meta object for the attribute '{@link henrik.studyPlan.Department#getNoOfCourses <em>No Of Courses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>No Of Courses</em>'.
	 * @see henrik.studyPlan.Department#getNoOfCourses()
	 * @see #getDepartment()
	 * @generated
	 */
	EAttribute getDepartment_NoOfCourses();

	/**
	 * Returns the meta object for the containment reference list '{@link henrik.studyPlan.Department#getCourses <em>Courses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Courses</em>'.
	 * @see henrik.studyPlan.Department#getCourses()
	 * @see #getDepartment()
	 * @generated
	 */
	EReference getDepartment_Courses();

	/**
	 * Returns the meta object for the containment reference list '{@link henrik.studyPlan.Department#getStudyPrograms <em>Study Programs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Study Programs</em>'.
	 * @see henrik.studyPlan.Department#getStudyPrograms()
	 * @see #getDepartment()
	 * @generated
	 */
	EReference getDepartment_StudyPrograms();

	/**
	 * Returns the meta object for the '{@link henrik.studyPlan.Department#addCourse(henrik.studyPlan.Course) <em>Add Course</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Course</em>' operation.
	 * @see henrik.studyPlan.Department#addCourse(henrik.studyPlan.Course)
	 * @generated
	 */
	EOperation getDepartment__AddCourse__Course();

	/**
	 * Returns the meta object for the '{@link henrik.studyPlan.Department#addStudyProgram(henrik.studyPlan.StudyProgram) <em>Add Study Program</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Study Program</em>' operation.
	 * @see henrik.studyPlan.Department#addStudyProgram(henrik.studyPlan.StudyProgram)
	 * @generated
	 */
	EOperation getDepartment__AddStudyProgram__StudyProgram();

	/**
	 * Returns the meta object for enum '{@link henrik.studyPlan.Specialization <em>Specialization</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Specialization</em>'.
	 * @see henrik.studyPlan.Specialization
	 * @generated
	 */
	EEnum getSpecialization();

	/**
	 * Returns the meta object for enum '{@link henrik.studyPlan.Term <em>Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Term</em>'.
	 * @see henrik.studyPlan.Term
	 * @generated
	 */
	EEnum getTerm();

	/**
	 * Returns the meta object for enum '{@link henrik.studyPlan.Degree <em>Degree</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Degree</em>'.
	 * @see henrik.studyPlan.Degree
	 * @generated
	 */
	EEnum getDegree();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>Course Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Course Code</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 * @generated
	 */
	EDataType getCourseCode();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	StudyPlanFactory getStudyPlanFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link henrik.studyPlan.impl.CourseImpl <em>Course</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see henrik.studyPlan.impl.CourseImpl
		 * @see henrik.studyPlan.impl.StudyPlanPackageImpl#getCourse()
		 * @generated
		 */
		EClass COURSE = eINSTANCE.getCourse();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__NAME = eINSTANCE.getCourse_Name();

		/**
		 * The meta object literal for the '<em><b>Code</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__CODE = eINSTANCE.getCourse_Code();

		/**
		 * The meta object literal for the '<em><b>Study Points</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__STUDY_POINTS = eINSTANCE.getCourse_StudyPoints();

		/**
		 * The meta object literal for the '<em><b>Owned By</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE__OWNED_BY = eINSTANCE.getCourse_OwnedBy();

		/**
		 * The meta object literal for the '{@link henrik.studyPlan.impl.SemesterImpl <em>Semester</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see henrik.studyPlan.impl.SemesterImpl
		 * @see henrik.studyPlan.impl.StudyPlanPackageImpl#getSemester()
		 * @generated
		 */
		EClass SEMESTER = eINSTANCE.getSemester();

		/**
		 * The meta object literal for the '<em><b>Semester No</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SEMESTER__SEMESTER_NO = eINSTANCE.getSemester_SemesterNo();

		/**
		 * The meta object literal for the '<em><b>Term</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SEMESTER__TERM = eINSTANCE.getSemester_Term();

		/**
		 * The meta object literal for the '<em><b>Specialization</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SEMESTER__SPECIALIZATION = eINSTANCE.getSemester_Specialization();

		/**
		 * The meta object literal for the '<em><b>Mandatory Courses</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEMESTER__MANDATORY_COURSES = eINSTANCE.getSemester_MandatoryCourses();

		/**
		 * The meta object literal for the '<em><b>Elective Courses</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEMESTER__ELECTIVE_COURSES = eINSTANCE.getSemester_ElectiveCourses();

		/**
		 * The meta object literal for the '<em><b>Add Mandatory Course</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SEMESTER___ADD_MANDATORY_COURSE__COURSE = eINSTANCE.getSemester__AddMandatoryCourse__Course();

		/**
		 * The meta object literal for the '<em><b>Add Elective Course</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SEMESTER___ADD_ELECTIVE_COURSE__COURSE = eINSTANCE.getSemester__AddElectiveCourse__Course();

		/**
		 * The meta object literal for the '{@link henrik.studyPlan.impl.StudyProgramImpl <em>Study Program</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see henrik.studyPlan.impl.StudyProgramImpl
		 * @see henrik.studyPlan.impl.StudyPlanPackageImpl#getStudyProgram()
		 * @generated
		 */
		EClass STUDY_PROGRAM = eINSTANCE.getStudyProgram();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STUDY_PROGRAM__NAME = eINSTANCE.getStudyProgram_Name();

		/**
		 * The meta object literal for the '<em><b>Code</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STUDY_PROGRAM__CODE = eINSTANCE.getStudyProgram_Code();

		/**
		 * The meta object literal for the '<em><b>Degree</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STUDY_PROGRAM__DEGREE = eINSTANCE.getStudyProgram_Degree();

		/**
		 * The meta object literal for the '<em><b>No Of Semesters</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STUDY_PROGRAM__NO_OF_SEMESTERS = eINSTANCE.getStudyProgram_NoOfSemesters();

		/**
		 * The meta object literal for the '<em><b>Semesters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STUDY_PROGRAM__SEMESTERS = eINSTANCE.getStudyProgram_Semesters();

		/**
		 * The meta object literal for the '<em><b>Department</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STUDY_PROGRAM__DEPARTMENT = eINSTANCE.getStudyProgram_Department();

		/**
		 * The meta object literal for the '{@link henrik.studyPlan.impl.DepartmentImpl <em>Department</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see henrik.studyPlan.impl.DepartmentImpl
		 * @see henrik.studyPlan.impl.StudyPlanPackageImpl#getDepartment()
		 * @generated
		 */
		EClass DEPARTMENT = eINSTANCE.getDepartment();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEPARTMENT__NAME = eINSTANCE.getDepartment_Name();

		/**
		 * The meta object literal for the '<em><b>No Of Courses</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEPARTMENT__NO_OF_COURSES = eINSTANCE.getDepartment_NoOfCourses();

		/**
		 * The meta object literal for the '<em><b>Courses</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPARTMENT__COURSES = eINSTANCE.getDepartment_Courses();

		/**
		 * The meta object literal for the '<em><b>Study Programs</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPARTMENT__STUDY_PROGRAMS = eINSTANCE.getDepartment_StudyPrograms();

		/**
		 * The meta object literal for the '<em><b>Add Course</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation DEPARTMENT___ADD_COURSE__COURSE = eINSTANCE.getDepartment__AddCourse__Course();

		/**
		 * The meta object literal for the '<em><b>Add Study Program</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation DEPARTMENT___ADD_STUDY_PROGRAM__STUDYPROGRAM = eINSTANCE.getDepartment__AddStudyProgram__StudyProgram();

		/**
		 * The meta object literal for the '{@link henrik.studyPlan.Specialization <em>Specialization</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see henrik.studyPlan.Specialization
		 * @see henrik.studyPlan.impl.StudyPlanPackageImpl#getSpecialization()
		 * @generated
		 */
		EEnum SPECIALIZATION = eINSTANCE.getSpecialization();

		/**
		 * The meta object literal for the '{@link henrik.studyPlan.Term <em>Term</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see henrik.studyPlan.Term
		 * @see henrik.studyPlan.impl.StudyPlanPackageImpl#getTerm()
		 * @generated
		 */
		EEnum TERM = eINSTANCE.getTerm();

		/**
		 * The meta object literal for the '{@link henrik.studyPlan.Degree <em>Degree</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see henrik.studyPlan.Degree
		 * @see henrik.studyPlan.impl.StudyPlanPackageImpl#getDegree()
		 * @generated
		 */
		EEnum DEGREE = eINSTANCE.getDegree();

		/**
		 * The meta object literal for the '<em>Course Code</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.String
		 * @see henrik.studyPlan.impl.StudyPlanPackageImpl#getCourseCode()
		 * @generated
		 */
		EDataType COURSE_CODE = eINSTANCE.getCourseCode();

	}

} //StudyPlanPackage
