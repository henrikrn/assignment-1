/**
 */
package henrik.studyPlan;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Study Program</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link henrik.studyPlan.StudyProgram#getName <em>Name</em>}</li>
 *   <li>{@link henrik.studyPlan.StudyProgram#getCode <em>Code</em>}</li>
 *   <li>{@link henrik.studyPlan.StudyProgram#getDegree <em>Degree</em>}</li>
 *   <li>{@link henrik.studyPlan.StudyProgram#getNoOfSemesters <em>No Of Semesters</em>}</li>
 *   <li>{@link henrik.studyPlan.StudyProgram#getSemesters <em>Semesters</em>}</li>
 *   <li>{@link henrik.studyPlan.StudyProgram#getDepartment <em>Department</em>}</li>
 * </ul>
 *
 * @see henrik.studyPlan.StudyPlanPackage#getStudyProgram()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='missingSemesters'"
 * @generated
 */
public interface StudyProgram extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see henrik.studyPlan.StudyPlanPackage#getStudyProgram_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link henrik.studyPlan.StudyProgram#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Code</em>' attribute.
	 * @see #setCode(String)
	 * @see henrik.studyPlan.StudyPlanPackage#getStudyProgram_Code()
	 * @model
	 * @generated
	 */
	String getCode();

	/**
	 * Sets the value of the '{@link henrik.studyPlan.StudyProgram#getCode <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Code</em>' attribute.
	 * @see #getCode()
	 * @generated
	 */
	void setCode(String value);

	/**
	 * Returns the value of the '<em><b>Degree</b></em>' attribute.
	 * The literals are from the enumeration {@link henrik.studyPlan.Degree}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Degree</em>' attribute.
	 * @see henrik.studyPlan.Degree
	 * @see #setDegree(Degree)
	 * @see henrik.studyPlan.StudyPlanPackage#getStudyProgram_Degree()
	 * @model
	 * @generated
	 */
	Degree getDegree();

	/**
	 * Sets the value of the '{@link henrik.studyPlan.StudyProgram#getDegree <em>Degree</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Degree</em>' attribute.
	 * @see henrik.studyPlan.Degree
	 * @see #getDegree()
	 * @generated
	 */
	void setDegree(Degree value);

	/**
	 * Returns the value of the '<em><b>No Of Semesters</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>No Of Semesters</em>' attribute.
	 * @see #setNoOfSemesters(int)
	 * @see henrik.studyPlan.StudyPlanPackage#getStudyProgram_NoOfSemesters()
	 * @model
	 * @generated
	 */
	int getNoOfSemesters();

	/**
	 * Sets the value of the '{@link henrik.studyPlan.StudyProgram#getNoOfSemesters <em>No Of Semesters</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>No Of Semesters</em>' attribute.
	 * @see #getNoOfSemesters()
	 * @generated
	 */
	void setNoOfSemesters(int value);

	/**
	 * Returns the value of the '<em><b>Semesters</b></em>' containment reference list.
	 * The list contents are of type {@link henrik.studyPlan.Semester}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Semesters</em>' containment reference list.
	 * @see henrik.studyPlan.StudyPlanPackage#getStudyProgram_Semesters()
	 * @model containment="true"
	 * @generated
	 */
	EList<Semester> getSemesters();

	/**
	 * Returns the value of the '<em><b>Department</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link henrik.studyPlan.Department#getStudyPrograms <em>Study Programs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Department</em>' container reference.
	 * @see #setDepartment(Department)
	 * @see henrik.studyPlan.StudyPlanPackage#getStudyProgram_Department()
	 * @see henrik.studyPlan.Department#getStudyPrograms
	 * @model opposite="studyPrograms" required="true" transient="false"
	 * @generated
	 */
	Department getDepartment();

	/**
	 * Sets the value of the '{@link henrik.studyPlan.StudyProgram#getDepartment <em>Department</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Department</em>' container reference.
	 * @see #getDepartment()
	 * @generated
	 */
	void setDepartment(Department value);

} // StudyProgram
