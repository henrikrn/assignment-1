/**
 */
package henrik.studyPlan;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Semester</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link henrik.studyPlan.Semester#getSemesterNo <em>Semester No</em>}</li>
 *   <li>{@link henrik.studyPlan.Semester#getTerm <em>Term</em>}</li>
 *   <li>{@link henrik.studyPlan.Semester#getSpecialization <em>Specialization</em>}</li>
 *   <li>{@link henrik.studyPlan.Semester#getMandatoryCourses <em>Mandatory Courses</em>}</li>
 *   <li>{@link henrik.studyPlan.Semester#getElectiveCourses <em>Elective Courses</em>}</li>
 * </ul>
 *
 * @see henrik.studyPlan.StudyPlanPackage#getSemester()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='mandatoryCoursesOverThirtyStudyPoints missingCoursesOverThirtyStudyPoints'"
 *        annotation="http://www.eclipse.org/acceleo/query/1.0 mandatoryCoursesOverThirtyStudyPoints='self.mandatoryCourses.studyPoints-&gt;sum() &lt;= 30.0' missingCoursesOverThirtyStudyPoints='self.electiveCourses.studyPoints-&gt;sum() + self.mandatoryCourses.studyPoints-&gt;sum() &gt;= 30.0'"
 * @generated
 */
public interface Semester extends EObject {
	/**
	 * Returns the value of the '<em><b>Semester No</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Semester No</em>' attribute.
	 * @see #setSemesterNo(int)
	 * @see henrik.studyPlan.StudyPlanPackage#getSemester_SemesterNo()
	 * @model
	 * @generated
	 */
	int getSemesterNo();

	/**
	 * Sets the value of the '{@link henrik.studyPlan.Semester#getSemesterNo <em>Semester No</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Semester No</em>' attribute.
	 * @see #getSemesterNo()
	 * @generated
	 */
	void setSemesterNo(int value);

	/**
	 * Returns the value of the '<em><b>Term</b></em>' attribute.
	 * The literals are from the enumeration {@link henrik.studyPlan.Specialization}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Term</em>' attribute.
	 * @see henrik.studyPlan.Specialization
	 * @see #setTerm(Specialization)
	 * @see henrik.studyPlan.StudyPlanPackage#getSemester_Term()
	 * @model
	 * @generated
	 */
	Specialization getTerm();

	/**
	 * Sets the value of the '{@link henrik.studyPlan.Semester#getTerm <em>Term</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Term</em>' attribute.
	 * @see henrik.studyPlan.Specialization
	 * @see #getTerm()
	 * @generated
	 */
	void setTerm(Specialization value);

	/**
	 * Returns the value of the '<em><b>Specialization</b></em>' attribute.
	 * The literals are from the enumeration {@link henrik.studyPlan.Term}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Specialization</em>' attribute.
	 * @see henrik.studyPlan.Term
	 * @see #setSpecialization(Term)
	 * @see henrik.studyPlan.StudyPlanPackage#getSemester_Specialization()
	 * @model
	 * @generated
	 */
	Term getSpecialization();

	/**
	 * Sets the value of the '{@link henrik.studyPlan.Semester#getSpecialization <em>Specialization</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Specialization</em>' attribute.
	 * @see henrik.studyPlan.Term
	 * @see #getSpecialization()
	 * @generated
	 */
	void setSpecialization(Term value);

	/**
	 * Returns the value of the '<em><b>Mandatory Courses</b></em>' reference list.
	 * The list contents are of type {@link henrik.studyPlan.Course}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mandatory Courses</em>' reference list.
	 * @see henrik.studyPlan.StudyPlanPackage#getSemester_MandatoryCourses()
	 * @model
	 * @generated
	 */
	EList<Course> getMandatoryCourses();

	/**
	 * Returns the value of the '<em><b>Elective Courses</b></em>' reference list.
	 * The list contents are of type {@link henrik.studyPlan.Course}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elective Courses</em>' reference list.
	 * @see henrik.studyPlan.StudyPlanPackage#getSemester_ElectiveCourses()
	 * @model
	 * @generated
	 */
	EList<Course> getElectiveCourses();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model CourseRequired="true"
	 * @generated
	 */
	void addMandatoryCourse(Course Course);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model CourseRequired="true"
	 * @generated
	 */
	void addElectiveCourse(Course Course);

} // Semester
