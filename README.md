# Assignment 1

The repo contains the ecore model, the xmi and the generated code w/manually written code.

### Some notes on the ecore-model:
* A department contains courses and studyprograms
* Each study program has a certain degree (enum) and semesters
* Each semester has an semesterNo attribute (year 1 containing semester 1 and 2 for example). Each semester also contains a specialization attribute.
* Each semester contains mandatory courses and elective courses. 
* Each course has a reference to a department, named "ownedBy". This reference is a opposite.

### The xmi:
* I included a constraint error to show the constraint working in the xmi

### The genmodel
* Used to generate source code

### Features used:
* classes and datatypes 
* operations -> check source code for the manually written functions
* attributes and references, both ordinary and containment references, oppsites -> opposites used on the relationship between a department and a course
* constraints (preferrably both manually written and with OCL)-> OCL used for checking constraints on a semester for checking if it's courses fulfill enough studypoints (over 30), as well as checking that mandatory courses don't overextend 30 points
* derived features -> noOfCourses is a derived feature which gets it's value from the size of the course list
